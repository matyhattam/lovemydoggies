<?php


namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_index")
     */
    public function index(): Response {
        return $this->render('/default/index.html.twig', ['page' => 1]);
    }
    /**
     * @Route("/conditions", name="default_conditions")
     */
    public function conditions(): Response {
        return $this->render('default/conditions.html.twig', ['page' => 2]);
    }
    /**
     * @Route("/contact", name="default_contact")
     */
    public function contact(): Response {
        return $this->render('default/contact.html.twig', ['page' => 3]);
    }
}
