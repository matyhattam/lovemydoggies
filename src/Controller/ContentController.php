<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends AbstractController
{
    /**
     * @Route("/annonces", name="contentPages_annonces")
     */
    public function annonces(): Response
    {
        $dogs = [
            [
                'name' => 'Médor',
                'img' => 'https://dogecoin.org/static/11cf6c18151cbb22c6a25d704ae7b313/dd8fa/doge-main.jpg',
            ],
            [
                'name' => 'Médoration',
                'img' => 'https://dogecoin.org/static/11cf6c18151cbb22c6a25d704ae7b313/dd8fa/doge-main.jpg',
            ]
        ];

        return $this->render('contentPages/annonces.html.twig',
            ['dogs' => $dogs]);
    }
}
